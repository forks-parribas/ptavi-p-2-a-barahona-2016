#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import calcparribas

class TestCalcParribas(unittest.TestCase):

    def setUp(self):
        self.calc = calcparribas.CalcCalls()

    def test_equal(self):
        self.assertTrue(3 == self.calc.calls)


if __name__ == '__main__':
    unittest.main()
