#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calccount


with open(sys.argv[1], "r") as file:
    for line in file:
        line = line.rstrip()
        list = line.split(",")

        operator = list[0]
        numbers = list[1:]
        result = 0
        total = 0

        object = calccount.Calc()
        if operator != '+' and operator != '-' and operator != '/' and operator != '*':
            print("Bad format")

        if numbers[-1] == "error":
            print("Bad format")
        else:
            if operator == "+":
                if len(numbers) == 1:
                    print(float(numbers[0]))
                else:
                    for i in numbers[0:]:
                        firstoperation = object.add(result, float(i))
                        total= total + firstoperation
                    print(total)

            elif operator == "-":
                if len(numbers) == 1:
                    print(numbers[0])
                elif len(numbers) == 2:
                    total = object.sub(float(numbers[0]), float(numbers[1]))
                    print(total)
                else:
                    firstoperation = float(numbers[0])-float(numbers[1])
                    for i in numbers[2:]:
                        total = object.sub(firstoperation, float(i))
                    print(total)

            if operator == "*":
                if len(numbers) == 1:
                    print(float(numbers[0]))
                else:
                    for i in numbers:
                        total = object.mul(float(numbers[0]), float(i))
                    print(total)

            if operator == "/":
                if numbers[1] == 0:
                    print("Error: division by zero")
                else:
                    firstoperation = float(numbers[0])/float(numbers[1])
                    for i in numbers[2:]:
                        if i == 0:
                            print("Error: division by zero")
                        else:
                            total = object.div(float(firstoperation), float(i))
                    print(total)